# Pràctiques I
__1. Còpia el fitxer test.py en el directori cgi-bin i visita’l amb el navegador: què fa?__
```
a ==> ['1', '3']
b ==> 8
```
> Aquesta script funciona tipus diccionari i ens dona el resultat de clau-valor de les dades introduïdes.

__2. Verifica aquest script amb diferents navegadors:__

+ __Firefox__
```
firefox 'http://localhost:8000/cgi-bin/test.py?a=1&a=3&b=8'
a ==> ['1', '3']
b ==> 8
```
> Obre el navegador firefox i ens dona el resultat.

+ __lynx__
```
lynx 'http://localhost:8000/cgi-bin/test.py?a=1&a=3&b=8'
a ==> ['1', '3']
b ==> 8
```
> Obre el seu propi navegador lynx i ens dona el resultat.

+ __elinks__
```
elinks 'http://localhost:8000/cgi-bin/test.py?a=1&a=3&b=8'
a ==> ['1', '3']
b ==> 8
```
> Obre el seu propi navegador elinks i ens dona el resultat.

+ __curl__
```
curl 'http://localhost:8000/cgi-bin/test.py?a=1&a=3&b=8'
a ==> ['1', '3']
b ==> 8
```
> Executa en terminal l'ordre i ens dona el resultat.

+ __wget__
```
wget 'http://localhost:8000/cgi-bin/test.py?a=1&a=3&b=8'
Saving to: ‘test.py?a=1&a=3&b=8’
test.py?a=1&a=3&b=8     [ <=>              ]      25  --.-KB/s    in 0s      
2020-02-05 08:28:00 (6.02 MB/s) - ‘test.py?a=1&a=3&b=8’ saved [25]
```
> Al executar l'ordre ens descarga el resultat de les dades.

__3. Executa aquest script directament en un terminal, definint les variables d’entorn necessàries.__

+ Executem les següents ordres per definir les variables d'entorn:
```
[isx46410800@localhost cgi-bin]$ export REQUEST_METHOD=get
[isx46410800@localhost cgi-bin]$ export QUERY_STRING="a=1&a=3&b=8"
```

+ Executem el script:
```
./test.py
Content-Type: text/plain; charset=UTF-8
a ==> ['1', '3']
b ==> 8
```

__4. Provoca en aquest script errors de sintaxi i errors en temps d’execució. Què passa?.__

+ Errors de sintaxi:
```
[isx46410800@localhost cgi-bin]$ ./test.py
Traceback (most recent call last):
  File "./test.py", line 14, in <module>
    form = cgi.FieldStorage()
NameError: name 'cgi' is not defined
[isx46410800@localhost cgi-bin]$ ./test.py
Content-Type: text/plain; charset=UTF-8
Traceback (most recent call last):
  File "./test.py", line 21, in <module>
    for k in forms:
NameError: name 'forms' is not defined
```
> Ens mostra per pantalla errors de no estar definides certes variables.

+ Error d'execució:
```
[isx46410800@localhost cgi-bin]$ ./test.py
Content-Type: text/plain; charset=UTF-8
a ==> None
b ==> None
m ==> None
```
> Ens mostra per pantalla errors d'estar malament executat amb valors none.

__5. Visita aquest script amb alguns dels formularis realitzats anteriorment.__

+ En el meu cas he posat el formulari de [pizzas](./website/UF3A1T1_pizzas.html) amb aquest script amb l'opció:
```
<form action="cgi-bin/test.py" method="POST">
```

+ El resultat:
```
burguer ==> ['pollo']
segundo ==> ['pollo']
submit ==> ['Aceptar']
servidor ==> ['server edt']
bebida ==> ['vino']
primero ==> ['arroz']
clienteId ==> ['100']
nombre ==> ['miguel']
serverId ==> ['300']
postre ==> ['flan']
burguers ==> ['huevo', 'bacon']
pizza ==> ['margarita', 'champi\xc3\xb1ones', 'anchoas']
```

__6. Estudia la documentació del mòdul cgi, especialment les propietats de l’objecte retornat per cgi.FieldStorage().__

- La funció _cgi.FieldStorage()_, retorna les dades en forma semblant a un diccionari.

- Les dades passades a la url després del ? ens retornarà amb el script uns resultats en forma de varible i el seu valor.

__7. Prepara un formulari i modifica aquest script per explorar el serveis proporcionats per aquest object.__
+ En el meu cas he posat el formulari de [login](./website/UF3A1T1_login.html) amb aquest script amb l'opció:
```
<form action="cgi-bin/test.py" method="POST">
```

+ El resultat:
```
login ==> login
pwd ==> miguel14
email ==> miguel14amoros@gmail.com
```

+ Canviant form.getvalue() per form.getlist():
```
login ==> ['login']
pwd ==> ['kjkhh']
email ==> ['miguel14amoros@gmail.com']
```

+ Canviant form.getvalue() per form.getfirst()
```
login ==> login
pwd ==> miguel
email ==> miguel14amoros@gmail.com
```

# Pràctiques II
##### Aquests exercicis demanen presentar diferents formularis, i han de ser processats per scripts que utilitzin el mòdul cgi:

__1. Calculadora de les quatre operacions bàsiques (+, −, × i ÷), a ser presentades en un selector desplegable: per defecte ha de fer sumes, i si no es proporcionen operands o aquests no es poden convertir al tipus float cal usar els valors neutres de l’operació demanada. La resposta del script ha de ser un document HTML amb el resultat de l’operació i un enllaç per tornar a la calculadora.__

+ [Formulari](./website/UF3A1T3_calculadora.html)
+ [Script](./website/cgi-bin/calculadora.py)

__2. Formulari amb selector múltiple per comunitats autònomes espanyoles: cal respondre amb el nombre d’habitants de cada comunitat i la suma total dels mateixos (usa aquests codis per identificar les comunitats en el formulari).__

+ [Formulari](./website/UF3A1T3_comunidades.html)
+ [Script](./website/cgi-bin/comunidades.py)

__3. Formulari per fer login: si la contrasenya no és correcte es còpia com a resposta de nou el formulari.__

+ [Formulari](./website/UF3A1T3_login.html)
+ [Script](./website/cgi-bin/login.py)

__4. Formulari per canvi de contrasenya: demanar dues vegades nova contrasenya i verificar que són iguals; en cas d’error actuar com en el cas anterior.__

+ [Formulari](./website/UF3A1T3_password.html)
+ [Script](./website/cgi-bin/password.py)

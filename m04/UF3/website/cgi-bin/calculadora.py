#!/usr/bin/python3
#Miguel Amorós

# Calculadora de les quatre operacions bàsiques (+, −, × i ÷), a ser
# presentades en un selector desplegable: per defecte ha de fer sumes,
# i si no es proporcionen operands o aquests no es poden convertir al
# tipus float cal usar els valors neutres de l’operació demanada. La
# resposta del script ha de ser un document HTML amb el resultat de
# l’operació i un enllaç per tornar a la calculadora.


import sys
import cgi

# Debug
#import cgitb; cgitb.enable()

write = sys.stdout.write

form = cgi.FieldStorage()

# headers
write("Content-Type: text/html; charset=UTF-8\r\n") # or "text/html"...
write("\r\n")

# conseguimos la lista de numeros a través de name="numero" del form
numeros = form.getlist("numero")
# conseguimos los dos numeros a través de la lista numeros
numero1 = float(numeros[0])
numero2 = float(numeros[1])

# conseguimos los valores del selector name="operacion" del form
calculo = form.getvalue("operacion")
# para cada valor, hacemos tal cosa
if calculo == "sumar":
    resul=numero1+numero2
elif calculo == "restar":
    resul=numero1-numero2
elif calculo == "multiplicar":
    resul=numero1*numero2
elif calculo == "dividir":
    resul=numero1/numero2
else:
    resul=numero1+numero2

print("El resultado es %s"%resul+'\n')
print('')
#redirectURL = "http://localhost:8000/UF3A1T3_calculadora.html"
print('<html>')
print('  <head><br><br>')
print('    <a href="http://localhost:8000/UF3A1T3_calculadora.html">Volver atrás</a>')
print('  </head>')
print('</html>')

sys.exit(0)

# vim:sw=4:ts=4:ai:et

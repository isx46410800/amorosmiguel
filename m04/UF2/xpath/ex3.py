#!/usr/bin/python
# -*- coding:utf-8-*-

# EJERCICIO3
## Mostra la lista de provincies i el total de municipis que te cadascuna.
from lxml import etree
arbol = etree.parse ('poblacions.xml')
provincias = arbol.xpath ('/lista/provincia/nombre')
municipis = arbol.xpath ('/lista/provincia/localidades/localidad')
raiz = arbol.getroot()

for provincia in provincias:
	num_municipis = provincia.xpath("count(../localidades/localidad)")
	print provincia.text, num_municipis
	
##USAR CON FIND
#provincias = arbol.xpath ('/lista/provincia')
#print "%s: %d localitats" %(provincia.find("nombre").text, num_municipis)

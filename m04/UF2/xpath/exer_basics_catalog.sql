--Exercicis basics de catalog
>>> from lxml import etree
>>> doc = etree.parse('cd_catalog.xml')
>>> print etree.tostring(doc,pretty_print=True ,xml_declaration=True, encoding="utf-8")
>>> raiz = doc.getroot()
>>> print raiz.tag


--1-mostrar el titulo del primer CD
>>> print raiz[0][0].text
Empire Burlesque

--2-Mostrar el titulo del segon CD
>>> print raiz[1][0].text
Hide your heart

--3-Mostrar titulo, artista, any del tercer CD
>>> print raiz[2][0].text, raiz[2][1].text, raiz[2][5].text
Greatest Hits Dolly Parton 1982
>>> print "titulo: ", raiz[2][0].text, ",", "artista: ", raiz[2][1].text, ",", "año: ", raiz[2][5].text
titulo:  Greatest Hits , artista:  Dolly Parton , año:  1982

print('titulo: %s, artista: %s, año: %s'%(x,x,x)) para una expresion de formato 
>>> print('titulo %s, artista %s, anyo %s' %(raiz[2][0].text, raiz[2][1].text, raiz[2][5].text))
titulo Greatest Hits, artista Dolly Parton, anyo 1982

--4-Numero de cds
>>> len(raiz)
26

--5-Numero de camps d'un cd
>>> len(raiz[0])
6

--6-Mostrar la estructura d'un cd
>>> for element in raiz[0]:
...     print element.tag
... 
TITLE
ARTIST
COUNTRY
COMPANY
PRICE
YEAR

>>> cd = doc.getroot()[0]
>>> for element in cd:
...     print(element.tag)
... 
TITLE
ARTIST
COUNTRY
COMPANY
PRICE
YEAR

--7-Mostrar tots el camps en camp:valor del tercer cd
>>> for element in raiz[2]:
...     print element.tag, ":", element.text
... 
TITLE : Greatest Hits
ARTIST : Dolly Parton
COUNTRY : USA
COMPANY : RCA
PRICE : 9.90
YEAR : 1982

--8-Ahora de todos los cds
>>> for cd in raiz:
...     for element in cd:
...             print element.tag, ":", element.text
... 
TITLE: Empire Burlesque
ARTIST: Bob Dylan
COUNTRY: USA
COMPANY: Columbia
...



# <u>Ejercicios de Xpath</u>

__1. Nombre de la Universidad.__
```
/universidad/nombre
Universidad de Victoria
```
__2. Pais de la Universidad.__
```
/universidad/pais o //pais
España
```
__3. Nombres de las Carreras.__
```
//carrera/nombre
I.T. Informática
Dipl. Empresariales
Dipl. Relaciones Laborales
Lic. Quimica
Lic. Biología
Lic. Humanidades
```
__4. Años de plan de estudio de las carreras.__
```
//carrera/plan
2003
2001
2001
2003
2001
1980
```
__5. Nombres de todos los alumnos.__
```
//alumnos//nombre o //alumnos/alumno/nombre
Víctor Manuel
Luisa
Fernando
María
```
__6. Identificadores de todas las carreras.__
```
//carrera/@id
c01
c02
c03
c04
c05
c06
```
__7. Datos de la carrera cuyo id es c01.__
```
//carrera[@id='c01']
I.T. Informática 2003 250 Escuela de Informática
```
__8. Centro en que se estudia de la carrera cuyo id es c02.__
```
//carrera[@id='c02']/centro
Facultad de Ciencias Sociales
```
__9. Nombre de las carreras que tengan subdirector.__
```
//carrera[subdirector]/nombre
Dipl. Relaciones Laborales
```
__10. Nombre de los alumnos que estén haciendo proyecto.__
```
//alumno//proyecto/../../nombre
Luisa
María
```
__11. Códigos de las carreras en las que hay algún alumno matriculado.__
```
//alumno//carrera/@codigo
c01
c02
c02
c01
```
__12. Apellidos y Nombre de los alumnos con beca.__
```
//alumno[@beca='si']/nombre|//alumno[@beca='si']/apellido1|//alumno[@beca='si']/apellido2
Pérez
Romero
Fernando
```
__13. Nombre de las asignaturas de la titulación c04.__
```
//asignatura[@titulacion='c04']/nombre
Pedagogía
Tecnología de los Alimentos
```
__14. Nombre de las asignaturas de segundo trimestre.__
```
//trimestre[.=2]/../nombre
Ingeniería del Software
Pedagogía
Didáctica
Tecnología de los Alimentos
Historia del Pensamiento
```
__15. Nombre de las asignaturas que no tienen 4 créditos teóricos.__
```
//creditos_teoricos[.!=4]/../nombre
Ofimática
Ingeniería del Software
Tecnología de los Alimentos
Bases de Datos
Historia del Pensamiento
```
__16. Código de la carrera que estudia el último alumno.__
```
//alumno[last()]//carrera/@codigo
c01
```
__17. Código de las asignaturas que estudian mujeres.__
```
//sexo[.='Mujer']/..//asignatura/@codigo
a02
a01
a02
a01
a07
```
__18. Nombre de los alumnos que matriculados en la asignatura a02.__
```
//asignatura[@codigo='a02']/../../../nombre
Luisa
Fernando
María
```
__19. Códigos de las carreras que estudian los alumnos matriculados en alguna asignatura.__
```
//alumno//asignatura/../../carrera/@codigo
c01
c02
c02
c01
```
__20. Apellidos de todos los hombres.__
```
//sexo[.='Hombre']/../apellido1|//sexo[.='Hombre']/../apellido2
Rivas
Santos
Pérez
Romero
```
__21. Nombre de la carrera que estudia Víctor Manuel.__
```
codigo carrera: //nombre[.='Víctor Manuel']/..//carrera/@codigo
//carrera/@id[.=//nombre[.='Víctor Manuel']/..//carrera/@codigo]/../nombre
I.T. Informática
```
__22. Nombre de las asignaturas que estudia Luisa.__
```
codigo de asignaturas: //nombre[.="Luisa"]/..//asignatura/@codigo
a02
a01
//asignatura/@id[.=//nombre[.="Luisa"]/..//asignatura/@codigo]/../nombre
Ofimática
Ingeniería del Software
```

__23. Primer apellido de los alumnos matriculados en Ingeniería del Software.__
```
id de la asignatura:
//nombre[.="Ingeniería del Software"]/../@id
a02
apellidos:
//asignatura/@codigo[.=//nombre[.="Ingeniería del Software"]/../@id]/../../../../apellido1
Pérez
Pérez
Avalón
```

__24. Nombre de las carreras que estudian los alumnos matriculados en la asignatura Tecnología de los Alimentos.__
```
id de la asignatura:
//nombre[.="Tecnología de los Alimentos"]/../@id
a07
codigo de carrera:
//@codigo[.=//nombre[.="Tecnología de los Alimentos"]/../@id]/../../../carrera/@codigo
c01
nombre:
//carrera/@id[.=//@codigo[.=//nombre[.="Tecnología de los Alimentos"]/../@id]/../../../carrera/@codigo]/../nombre
I.T. Informática
```

__25. Nombre de los alumnos matriculados en carreras que no tienen subdirector.__
```
con subdirector:
//subdirector/../@id
c03
//carrera/@codigo[.!=//subdirector/../@id]/../../../nombre
Víctor Manuel
Luisa
Fernando
María
```

__26. Nombre de las alumnos matriculados en asignaturas con 0 créditos prácticos y que estudien la carrera de I.T. Informática.__
```
asignatura:
//creditos_practicos[.=0]/../@id
//asignatura[creditos_practicos=0]/@id
a09
carrera:
//@id[.="c01"]
c01
nombre alumnos:
//asignatura[@codigo=//asignatura[creditos_practicos=0]/@id]/../../carrera[@codigo=//carrera[nombre="I.T. Informática"]/@id]/../../nombre
Víctor Manuel
```
__27. Nombre de los alumnos ques estudian carreras cuyos planes son anteriores a 2002.__
```
carreras con plan anteriores a 2002:
//plan[.<2002]/../@id
c02
c03
c05
c06
nombre de los alumnos:
//carrera/@codigo[.=//plan[.<2002]/../@id]/../../../nombre
Luisa
Fernando
```

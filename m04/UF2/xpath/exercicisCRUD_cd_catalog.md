# EXERCICIS CRUD
### __Exemple de classe: modificar el camp preu dels cd anteriors al any 1990. Pujar un +10%__  
```
//YEAR[.<"1990"]/../PRICE
>>> price = root.xpath("//YEAR[.<"1990"]/../PRICE")
>>> for precio in price:
...     precio.text = str(float(precio.text) + (float(precio.text)*0.1))
```

### __1. Afegir un nou cd amb tots els camps més genere.__  
```
>>> from lxml import etree
>>> root = etree.parse('cd_catalog.xml')
>>> catalog = root.getroot()
>>> cd = etree.SubElement(catalog, "CD")
>>> print(etree.tostring(catalog))

>>> title = etree.SubElement(cd, "TITLE").text="Otra vez"
>>> artist = etree.SubElement(cd, "ARTIST").text="Justin Quiles"
>>> country = etree.SubElement(cd, "COUNTRY").text="Puerto Rico"
>>> company = etree.SubElement(cd, "COMPANY").text="Rich Music"
>>> price = etree.SubElement(cd, "PRICE").text="10.0"
>>> year = etree.SubElement(cd, "YEAR").text="2019"
>>> genre = etree.SubElement(cd, "GENRE").text="Reggaeton"
```

### __2. Pujar el preu dels cds un 10 %__  
```
>>> precios = root.xpath("//PRICE")
>>> for precio in precios:
...     precio.text = str(float(precio.text) + (float(precio.text)*0.1))
...
```

### __3. Eliminar el CD One night only__  
//TITLE[.="One night only"]/..
```
>>> onenight = root.xpath("//TITLE[.=\"One night only\"]/..")
>>> for cd in root.xpath("//TITLE[.=\"One night only\"]/.."):
...     cd.getparent().remove(cd)
```

### __4. Eliminar els CDs de la companyia Polydor__  
//COMPANY[.="Polydor"]/..
```
>>> polydor = root.xpath("//COMPANY[.=\"Polydor\"]/..")
>>> for cd in polydor:
...     cd.getparent().remove(cd)
...
```

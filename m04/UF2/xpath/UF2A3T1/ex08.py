#!/usr/bin/python
# -*- coding: utf-8 -*-
#Comptar el nombre d’elements continguts en el document d’entrada.
import sys
import xml.etree.ElementTree as ET

# load
tree = ET.parse(sys.stdin)

raiz = tree.getroot()

elementos=0

# iteramos cada elemento y lo contamos
for element in tree.iter():
	elementos +=1
#mostramos resultado	
print("El numero de elementos es: ", elementos)		
	
# save
#tree.write(sys.stdout)

sys.exit(0)

# vim:sw=4:ts=4:ai:et

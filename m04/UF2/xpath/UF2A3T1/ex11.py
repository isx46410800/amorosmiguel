#!/usr/bin/python
# -*- coding: utf-8 -*-
#Comptar el nombre d’elements sense fills (sense altres elements dins seu).
import sys
import xml.etree.ElementTree as ET

# load
tree = ET.parse(sys.stdin)

raiz = tree.getroot()

elementosSinHijos=0

# iteramos cada elemento raiz Vemos los elementos hijos y contamos lo sin subelementos
for element in tree.iter():
	count=0
	for subElement in element:
		count+=1
	if count==0:
		elementosSinHijos+=1

print("El numero de elementos sin hijos es: ", elementosSinHijos)		
	
# save
#tree.write(sys.stdout)

sys.exit(0)

# vim:sw=4:ts=4:ai:et

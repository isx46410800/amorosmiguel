#!/usr/bin/python3
# -*- coding: utf-8 -*-

from lxml import html
import xml.etree.ElementTree as ET
import requests


# página de la cual partimos:
page = requests.get('https://formula1.lne.es/pilotos-f1/')

# Construcción del arbol html
tree = html.fromstring(page.content)
root = ET.Element("automovilismo")
escuderias = ET.SubElement(root, "escuderias")
pilotos = ET.SubElement(root, "pilotos")

#Esto creará lista de escuderias
lista_escuderias = tree.xpath("//div[@class=\"escuderia_piloto subtitle\"]")

#Esto creará lista de pilotos
lista_pilotos = tree.xpath("//span[@class=\"apellidos\"]")

#Esto creará la lista de puntuacions de constructores
l_puntuacion = tree.xpath("//td[@class=\"text-right col-xs-2\"]/div[@class=\"puntos_puesto\"]")

#Printamos cada escuderia sin repetir bien lo añadimos al arbol
lista_printadas=[]
print("\n"+"ESCUDERIAS FORMULA 1:"+"\n")
for escuderia in lista_escuderias:
	if escuderia.text not in lista_printadas:
		lista_printadas.append(escuderia.text)

for k in lista_printadas:
	ET.SubElement(escuderias, "escuderia").text='%s' % k.strip()
	print("\t"+k.strip())
#separacion	
print("")
print("")

#Pritamos cada piloto y tambien lo añadimos al arbol
print("PILOTOS FORMULA 1:"+"\n")
for piloto in lista_pilotos:
	ET.SubElement(pilotos, "piloto").text='%s' % piloto.text
	print("\t"+piloto.text)

print("")

#Puntuación maxima del ganador	
maximo=0
for puntos in l_puntuacion:
	puntos=int(puntos.text)
	if puntos>maximo:
		maximo=puntos

print("La escuderia ganadora de este año ha conseguido una puntuacion de %s puntos."%maximo+"\n")

#Generamos arbol y fichero xml
doc = ET.ElementTree(root)
doc.write("formula1.xml",encoding='UTF-8',xml_declaration=True)

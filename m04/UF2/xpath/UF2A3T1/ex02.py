#!/usr/bin/python
# -*- coding: utf-8 -*-
# Afegir a tots els elements un atribut de nom id amb un valor diferent per cada element.

import sys
import xml.etree.ElementTree as ET

# load
tree = ET.parse(sys.stdin)

# para cada elemento plant añadimos un id diferente
numero=1
for element in tree.iter():
		if element.tag == "PLANT":
			element.set("id", str(numero))
			#Tambien: element.attrib["id"] = str(numero)
			numero+=1
			

# save
tree.write(sys.stdout)

sys.exit(0)

# vim:sw=4:ts=4:ai:et

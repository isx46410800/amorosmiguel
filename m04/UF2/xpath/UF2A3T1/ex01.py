#!/usr/bin/python
# -*- coding: utf-8 -*-
# Eliminar tots els atributs d’un document.

import sys
import xml.etree.ElementTree as ET

# load
tree = ET.parse(sys.stdin)

# eliminamos cada atributo vaciandolo
for element in tree.iter():
		element.attrib = {}

# save
tree.write(sys.stdout)

sys.exit(0)

# vim:sw=4:ts=4:ai:et

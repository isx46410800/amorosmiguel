from lxml import html
import requests

# https://docs.python-guide.org/scenarios/scrape/
page = requests.get('http://econpy.pythonanywhere.com/ex/001.html')

# We need to use page.content rather than page.text because html.fromstring
# implicitly expects bytes as input.
tree = html.fromstring(page.content)

#This will create a list of buyers:
#llista_compradors_preus  = tree.xpath('//div[@title="buyer-name"]/text()|//span[@class="item-price"]/text()')
llista_compradors_preus  = tree.xpath('//div[@title="buyer-info"]')
#This will create a list of prices

for buyer in llista_compradors_preus:
    print ("Buyer: {} price: {}".format(buyer[0].text, buyer[1].text))

#!/usr/bin/python
# -*- coding:utf-8-*-

# EJERCICIO1
#Actualitar el processador i7-6700 per un AMD Ryzen 7 2700 4.1 Ghz 
#(utilitzeu la funció de xpath contains). Si la informació hi és a més
#d'un element, s'ha d'actualitzar a tot arreu.
# -----
# [isx46410800@i05 xpath]$ cat fitxersortida.xml | grep "i7"
     # <product>Intel(R) Core(TM) i7-6700 CPU @ 3.40GHz</product>
     # <version>Intel(R) Core(TM) i7-6700 CPU @ 3.40GHz</version>

# -----
from lxml import etree
arbol = etree.parse ('fitxersortida.xml')
componentes = arbol.getroot()
#placa_mare_list = componentes.xpath("//*[contains(text(),\"i7-6700\"])")
product = componentes.xpath("//list//product[contains (text(),'i7-6700')]")
version = componentes.xpath("//list//version[contains (text(),'i7-6700')]")

product[0].text = "AMD Ryzen 7 2700 4.1 Ghz"
version[0].text = "AMD Ryzen 7 2700 4.1 Ghz"
arbol.write("fitxersortida_2.xml")








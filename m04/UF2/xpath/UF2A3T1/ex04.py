#!/usr/bin/python
# -*- coding: utf-8 -*-
# Eliminar de tots els elements un determinat atribut, rebent el seu nom com argument (a sys.argv[]).

import sys
import xml.etree.ElementTree as ET

# load
tree = ET.parse(sys.stdin)

atributo = sys.argv[1]

# para cada etributo de sys.argv, eliminamos el tag
for element in tree.iter():
	if atributo in element.attrib:
		del(element.attrib[atributo])
	
# save
tree.write(sys.stdout)

sys.exit(0)

# vim:sw=4:ts=4:ai:et

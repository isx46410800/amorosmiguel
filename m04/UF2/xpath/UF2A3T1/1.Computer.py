#!/usr/bin/python
# -*- coding:utf-8-*-

# EJERCICIO1
#Inserir un nou disc dur marca SEAGATE de 512 GB (fixeu-vos en quines unitats s'expressa al dataset).

from lxml import etree
arbol = etree.parse ('fitxersortida.xml')
componentes = arbol.getroot()
#XPATH nos da una lista, entonces para asignar luego al elemento padre[0]
padre_disco = componentes.xpath("//node[@id=\"disk\"]/..")
#añadimos el subelemento al elemento padre[0], porque generaba una lista
hd = etree.SubElement(padre_disco[0], "node")
hd.set("id","disk")
hd.set("claimed","true")
hd.set("class","disk")

etree.SubElement(hd, "description").text="ATA DISK"
etree.SubElement(hd, "product").text="SEAGATE"
etree.SubElement(hd, "size").text="512000000000"

arbol.write("fitxersortida_1.xml")








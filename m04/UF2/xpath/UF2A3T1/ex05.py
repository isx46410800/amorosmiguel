#!/usr/bin/python
# -*- coding: utf-8 -*-
# Prefixar els noms dels elements amb una cadena, rebuda com argument (a sys.argv[].

import sys
import xml.etree.ElementTree as ET

# load
tree = ET.parse(sys.stdin)

prefijo = sys.argv[1]

# para cada elemento, le añadimos un prefijo de cadena al texto
for element in tree.iter():
	element.tag = prefijo + element.tag
	
# save
tree.write(sys.stdout)

sys.exit(0)

# vim:sw=4:ts=4:ai:et

#COMPROBACION
#$ python ex05.py "El prefijo es " < plant_catalog.xml 
#<COMMON>El prefijo es Cardinal Flower</COMMON>

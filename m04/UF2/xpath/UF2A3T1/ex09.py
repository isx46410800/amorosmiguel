#!/usr/bin/python
# -*- coding: utf-8 -*-
#Comptar el nombre d’atributs continguts en el document d’entrada.
import sys
import xml.etree.ElementTree as ET

# load
tree = ET.parse(sys.stdin)

raiz = tree.getroot()

atributos=0

# iteramos cada elemento. Vemos los elementos con atributos y lo contamos
for element in tree.iter():
	if element.attrib:
		atributos = len(element.attrib) + atributos
#mostramos resultado	
print("El numero de atributos es: ", atributos)		
	
# save
#tree.write(sys.stdout)

sys.exit(0)

# vim:sw=4:ts=4:ai:et

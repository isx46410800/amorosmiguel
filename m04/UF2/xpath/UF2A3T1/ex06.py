#!/usr/bin/python
# -*- coding: utf-8 -*-
# Verificar que tots els elements fills de l’element arrel tenen un determinat nombre de fills, rebent aquest nombre com argument (a sys.argv[]).
import sys
import xml.etree.ElementTree as ET

# load
tree = ET.parse(sys.stdin)

raiz = tree.getroot()
numero_fills = int(sys.argv[1])

# para cada elemento, comprobamos que tiene la cantidad de hijos de sys.argv
##por cada hijo de la raiz
for fill in raiz:
	count=0
	#por cada subhijo ,contar sus hijos
	for subfill in fill:
		count+=1
	#si es igual al numero de fills
	if count==numero_fills:
		print("Correcto")
	else:
		print("No tiene %d subelementos"%(numero_fills))

		
		
	
# save
#tree.write(sys.stdout)

sys.exit(0)

# vim:sw=4:ts=4:ai:et


#!/usr/bin/python
# -*- coding: utf-8 -*-
#Comptar el nombre de caràcters de text continguts en document d’entrada.
import sys
import xml.etree.ElementTree as ET

# load
tree = ET.parse(sys.stdin)

raiz = tree.getroot()

letras=0

# iteramos cada elemento. De cada elemento texto, iteramos cada letra y la contamos
for element in tree.iter():
	if element.text:
		for letra in element.text:
			letras +=1
#mostramos resultado	
print("El numero de letras es: ", letras)		
	
# save
#tree.write(sys.stdout)

sys.exit(0)

# vim:sw=4:ts=4:ai:et

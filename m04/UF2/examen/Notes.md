# Notes 
Alumne: Amorós, Miguel

Exercici|Nota|Nota obtinguida|Comentari
-|-|:-:|-
1|1| |
2|1| |
3|1| |
4|1| |
5|1| |

4.5 de la part escrita


Exercici|Nota|Nota obtinguida|Comentari
-|-|:-:|-
6|1|1|
7|1|1|
8|1|0.75| Has usat un for en lloc d'indexar!
9|1|0.25|Es demana el nom del mòdul, no el codi. Només estàs controlant que l'hora sigui anterior o igual a les 9. I si Comença ales 9 i només dura una hora? I si comença a les 7 un mòdul?
10|1|1|Has complicat molt el codi
Nota| Final| 8.50 |A partir de l'exercici 7 inclòs has fet write!
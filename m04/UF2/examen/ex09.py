#!/usr/bin/python
# -*- coding:utf-8-*-
#Miguel Amoros
#Profe que imparteixen dos asignatures
import sys
from lxml import etree
root = etree.parse ('institut_examen.xml')

lista_horas = root.xpath("//Horari")

#creamos un diccionario para los profes
diccionari_profes = {}

#miramos cada atributo de professor y lo vamos añadiendo segun esté o no
for horas in lista_horas:
    if horas.attrib["professor"] not in diccionari_profes:
        diccionari_profes[horas.attrib["professor"]] = [horas.attrib["modul"]]
    else:
        if horas.attrib["modul"] not in diccionari_profes[horas.attrib["professor"]]:
            diccionari_profes[horas.attrib["professor"]].append(horas.attrib["modul"])
#Si tienen mas de dos, printamos el profesor
for profe in diccionari_profes:
    if len(diccionari_profes[profe]) > 1:
        id_profe = "//Professor[@id=\"%s\"]/Nom" % profe
        print root.xpath(id_profe)[0].text
        

root.write("institut_examen.xml")

sys.exit(0)


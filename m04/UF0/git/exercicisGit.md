# <u>EXERCICIS GIT</u>

## NIVELL 1

##### 1. Realitza dos commits per passar de nivell  
Tenim el commit C1 i C2, fem un `git commit` dues vegades i crearem en la branca dos commits més per realitzar la tasca.

##### 2. Realitza la creació d'una nova branca
- Amb `git branch novabranca` creem una nova branca i ara el head apunta aquesta branca al l'últim commit fet.
- Amb aquesta comanda noves creem la branca, ens mantenim a la branca master. Per canviar de branca fem un `git checkout nombranca`.
- Per crear una nova branca i canviar-nos directament a aquesta fem un `git checkout -b nombranca`  
En la tasca farem:
```
git checkout -b bugFix
```

##### 3. Realitza el merge de dues branques
En aquesta tasca farem la fusió, merge, de les branques master i bugFix.
Farem:
```
git branch -b bugFix
```
```
git checkout bugFix
```
```
git commit
```
```
git checkout master
```
```
git commit
```
```
git merge bugFix
```

##### 4. Realitza un rebase de dues branques
En aquesta tasca farem una fusió de dues branques amb el mètode `git rebase`.  
Farem:
```
git branch -b bugFix
```
```
git checkout bugFix
```
```
git commit
```
```
git checkout master
```
```
git commit
```
```
git checkout bugFix
```
```
git rebase master
```

## NIVELL 2

##### 5. Realitza un canvi de head
Per fer aquesta tasca utilitzarem una comanda que ens canvia de HEAD en la rama que vulguem estar:
```
git checkout nombranca
```
```
git checkout numCommit
```

En el nostre cas farem `git checkout C4`

#### 6. Realitza canvis de HEAD d'una altre branca
```
git checkout nombranca
```
```
git checkout HEAD^
```

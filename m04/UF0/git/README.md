# GIT
### Organització les zones de treball i conceptes bàsics

__GIT__ és un sistema de serveis de control (VCS).

S'organitza en les següents zones de treball:

Working Directory|Staging Area|Local Repo|Remote Repo
:-:|:-:|:-:|:-:|
git add|git commit|git push|

Com podem veure hi ha 4 zones de treball: el <u>directori de treball</u>, que per passar
a la següent zona s'utilitza el __git add__; la <u>staging area</u> o també es pot dir
index, que per passar a la següent zona s'utilitza el __git commit__; la <u>local repo</u>,
que per passar a la següent zona s'utilitza el __git push__ per arribar finalment
a <u>remote Repo</u>.

Estats d'un fitxer a GIT:
+ **Untracked:** no seguit(no rastrejat).
+ **Staged:** preparat, seguit.
+ **Modified:** fitxer no actualitzat com l'últim commit.
+ **Commited:** confirmat

### Creació inicial

Per fer la creació inicial d'un repositori GIT fem els següents passos:
1. Creem el directori en local.
2. Fem un **git init** en aquest directori per iniciar el repositori buit, on tindrem un directori __.git__.

> Aquest directori _.git_ conté tota la informació del repositori com tota la seva estructura de branques.

### Sincronització local/remot

1. En el nostre compte de Git creem un nou projecte on tindrem la nostra direcció HTTPS del repositori remot creat, com por exemple: https://gitlab.com/m04-alumnes/amorosmiguel.git
2. En el directori local fem un __<u>git remote add origin https://gitlab.com/m04-alumnes/amorosmiguel.git</u>__
3. Comprovem amb un __git remote -v__ on veurem la branca per defecte (_master_) i el repo remot (_origin_).
4. Finalment, per pujar per primer cop haurem de fer __git push -u origin master__ per a terminar la sincronització.

> **Master** és la branca principal per defecte que es crea al fer un repositori GIT.  
**Origin** és el repositori origen per defecte.


### Ús de claus per facilitar la pujada

Per poder fer una sincronització del treball fet per exemple d l'escola amb la nostra casa i no tenir que demanar l'autenticació sempre, fem l'ús de les claus públiques i privades.

Passos a seguir:
+ Tenir fet el directori local amb el .git
+ A [GitLab](https://gitlab.com) anem a profile - settings - SSH Keys.
+ Cliquem a crear keys.
+ En el nostre terminal generem las keys amb la comanda:
    - ssh-keygen -o -t rsa -b 4096 -C "email@example.com"
+ Ens indicà els fitxers on tenim las claus i agafarem la clau pública per a la sincronització:
    - .ssh/id_rsa
+ Comprovem amb la comanda _ssh -T git@gitlab.com_
+ Copiem la clau i la posem en gitlab i ja tenim les claus SSH configurades.
+ Aquests passos s'han de fer en tots els ordinadors on volem treballar-hi amb el repositori GitLab sense demanar autenticació.


### Operacions amb el remot (add, delete)

Podem configurar el origin i master per afegir o esborrar-ho de la següent manera:  

+ git remote add origin https://gitlab.com/m04-alumnes/amorosmiguel.git
> Pot ser per HTTPS o SSH  

+ git remote remove origin  

<u>NOTA:</u> Si es configura les claus SSH, quan fem algun push, no ens demanarà l'autenticació.


### Creació de branques, canvi entre branques, fusions i esborrats
Una __branca__ és un flutxe de treball el qual conté el apuntador HEAD que senyala a l'últim commit fet.

__Master__ es diu a la branca per defecte quan fem git init però només existirà al fer el primer commit, ja que una branca és una suma de commits. No fa falta que aquest directori sigui remot.

Abans de la creació de les branques tenim dos aspectes a comentar:
+ __git log:__ veiem tota la llista de commits realitzats amb el seu hash i descripció.
+ __HEAD:__ apunta a l'últim commit de la branca actual.
+ __git show HASH:__ per veure tots els canvis de aquell commit en concret.

###### Creació de branques:
+ git branch NOMBRANCA
+ git checkout -b NOMBRANCA

> La diferència és que la primera es crea una branca però es manté en la actual i la segona es crea una nova branca i es canvia a aquesta branca creada.

###### Canvi entre branques:
+ git checkout NOMBRANCA

###### Esborrar branques:
+ git branch -d NOMBRANCA
+ git branch -D NOMBRANCA

> La diferència és que la primera esborra la branca si té l'arbre net i la segona esborra forçadament encara que hi hagi fitxers diferents entre branques.

###### Fusionar branques:
+ Canviem de la branca master a la nova : git checkout NOMBRANCA
+ Modifiquem fitxers, fem add i commit.
+ Canviem de nou a la branca master: git checkout master
+ Fusionem: git merge NOMBRANCA

> Hem de tenir en compte que si no es va commit en la nova branca, els canvis els veuen en totes les branques per igual.


### Desfer canvis, en els casos més habituals:

+ Fitxers no seguits
  - Posar els fitxers en un .ignore
  - git rm file
+ Desfer canvis afegits a l'index:
  - git reset HEAD
  - git checkout -- file (Per arxius modificats)
+ Desfer un commit
  - git reset HEAD~1/file
+ Sobreescriure un commit:
  - git commit --amend

### Qualsevol punt que considereu interessant

- A l'hora de la creació del repositori remot, una vegada hem comprovat amb el git remote -v, podem configurar el nom de user i e-mail.
Hem de fer les ordres  següents:
    + git config --global user.name NOM
    + git config --global user.email EMAIL


- Podem ignorar arxius que a l'hora de fer un commit ens indiqui que tenim alguna cosa sense trackejar, és a dir, no tenim l'arbre net.  
Per fer això:
    1. Creem en el directori local un .ignore: __vim .ignore__
    2. Dins fiquem tots el noms del fitxers que volem ignorar.
    3. Guardem i al fer el commit de nou, veurem que tindrem l'arbre net.

- Per veure els estats del nostre directori git fem __git status__ però també podem utilitzar la forma curta amb __git status -s/--short.__

- Fer un __fork__ serveix per a fer una còpia de qualsevol repositori però posant-lo al teu nom d'usuari.  
Per exemple d'un fork:
    1. Agafem el repositori m04 del profe i cliquem a fer un fork amb el nostre nom.
    2. El clonem al nostre directori i comprovem amb git remote -v que està amb el nostre nom d'usuari.
    3. Si fem un __git remote -v <u>m04-profe</u> <u>direcció HTTPS</u>__, farem en el nostre directori que tinguem el repositori original del profe i el nostre on treballarem per fer coses noves.


- Fer un __fetch__ significa baixar-se al nostre directori només les coses que han canviat respecte l'última baixada. A diferència de clonar, que es carrega tot el directori, es destructiu.  
Seguint l'exemple:
    1. git fetch m04-profe master : ens sortirà al directori nostre l'ho nou del profe.
    2. git checkout master, per canviar i treballar a la nostra branca.
    3. git merge m04-profe, s'afegeix tot l'ho nou a nostre branca de nostre repositori.

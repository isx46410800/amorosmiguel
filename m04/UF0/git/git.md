# GIT

__GIT__ és un sistema de serveis de control (VCS).

S'organitza en les següents zones de treball:

![](aux/zones_treball.png)


# Estats d'un fitxer

+ **Untracked:** no seguit(no rastrejat).
+ **Staged:** preparat, seguit.
+ **Modified:** fitxer no actualitzat com l'últim commit.
+ **Commited:** confirmat


# Ordres per passar d'un estat a l'altre

+ De **Untracked** a **Staged:**
```
git add file/.
```
+ De **Staged** a **Commited:**
```
git commit -m "Description"
```
+ De **Commited** al **Repositori Remot:**
```
git push
```

# Creació inicial d'un repositori:

1. Creem el directori en local.
2. Fem un **git init**

__NOTA:__ _Es crearà el directori ocult _**_.git_** _amb la info i estructura del repositori._


# Sincronització local/remot

1. Creació del projecte a GitLab.
```
Exemple:  
https://gitlab.com/m04-alumnes/amorosmiguel.git
```
2. En el directori local:
```
git remote add origin NomRepositoriRemot
```
3. Comprovem amb un __git remote -v__
4. Primera pujada __git push -u origin master__


# Origin/Master

+ **Master** és la branca principal per defecte que es crea al fer un repositori GIT.  
+ **Origin** és el repositori origen per defecte.


# Ús de claus per facilitar la pujada

Per una millor sincronització del treball fem l'ús de les claus públiques i privades.

Passos a seguir:  

1. Tenir fet el directori local amb el .git  
2. A [GitLab](https://gitlab.com) anem a profile - settings - SSH Keys.  
3. Cliquem a crear keys.  


# __Veiem aquests passos:__

![](aux/keys1.png)

#

+ En el nostre terminal generem las keys amb la comanda:
```
ssh-keygen -o -t rsa -b 4096 -C "email@example.com"
```
+ Ens indicà els fitxers on tenim las claus i agafarem la clau pública per a la sincronització:
```
.ssh/id_rsa
```

![](aux/keys2.png)

#

+ Copiem la clau i la posem en gitlab i ja tenim les claus SSH configurades.

![](aux/keys3.png)

__NOTA:__ _Aquests passos s'han de fer en tots els ordinadors on volem treballar-hi amb el repositori GitLab._


# Operacions amb el remot (add, delete)

Podem configurar el origin i master per afegir o esborrar-ho de la següent manera:  

```
git remote add origin NomRepositoriRemot*
```
```
git remote remove origin  
```

\* Pot ser per HTTPS o SSH  


# Creació de branques:

```
git branch NOMBRANCA
git checkout -b NOMBRANCA
```

__NOTA:__ _La primera crea una nova però es queda en l'actual i l'altre crea i canvia de branca._


# Canvi entre branques:

```
git checkout NOMBRANCA
```

# Esborrar branques:

```
git branch -d NOMBRANCA
git branch -D NOMBRANCA
```

__NOTA:__ _La primera esborra la branca si té l'arbre net i la segona esborra forçadament_


# CLONE/FETCH/MERGE/FORK

+ __CLONE:__ Descarrega tot de manera destructiva en el directori.

+ __FETCH:__ Descarrega tot en la branca oculta, no destructiva.

+ __FORK:__ Va una còpia d'un repositori canviant-lo al teu nom.

+ __MERGE:__ Uneix el treball fet a una altre branca, amb la branca master.


# Desfer canvis, en els casos més habituals:

+ Fitxers no seguits
```
Posar els fitxers en un .ignore
git rm file
```
+ Desfer canvis afegits a l'index:
```
git reset HEAD
git checkout -- file (Per arxius modificats)
```
+ Desfer un commit:
```
git reset HEAD~1
```
+ Sobreescriure un commit:
```
git commit --amend
```

# Altres comandes treballades

- Per configurar dades d'un repositori:
```
git config --global user.name NOM
git config --global user.email EMAIL
```
- Per ignorar fitxers pendents de trackejar:
```
vim .ignore
```
- Per veure els estats del fitxers del meu repositori:
```
git status / git status -s/--short
```


# GIT

![](aux/git.png)

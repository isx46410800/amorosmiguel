# <u>PRÀCTICA 4: NAMESPACES</u>

##### Indiqueu quins són els espais de noms i els prefixos definits en aquest fitxer, explicar perquè serveixen. Mira si en algun cas és possible prescindir d'algun àlies.

```
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    >
    <xsl:template match="doc">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4" page-width="297mm" page-height="210mm" margin-top="1cm" margin-bottom="1cm" margin-left="1cm" margin-right="1cm">
                    <fo:region-body margin="3cm"/>
                    <fo:region-before extent="2cm"/>
                    <fo:region-after extent="2cm"/>
                    <fo:region-start extent="2cm"/>
                    <fo:region-end extent="2cm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="A4" format="A">
                <fo:flow flow-name="xsl-region-body">
                    <xsl:apply-templates select="para"/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    <xsl:template match="para">
        <fo:block><xsl:value-of select="."/></fo:block>
    </xsl:template>
    <xsl:template match="para[1]">
        <fo:block><fo:inline font-weight="bold"><xsl:value-of select="."/></fo:inline></fo:block>
    </xsl:template>
</xsl:stylesheet>
```
+ __Espais de noms:__
  - xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  - xmlns:fo="http://www.w3.org/1999/XSL/Format"  


+ __Prefixos:__
  - XSL:
    - xsl:stylesheet
    - xsl:template
  - FO:
    - fo:root
    - fo:layout-master-set
    - fo:simple-page-master
    - fo:region-body
    - fo:region-before
    - fo:region-after
    - fo:region-start
    - fo:region-end
    - fo:page-sequence
    - fo:flow
    - fo:block

  > Els prefixos que s'assignen amb __xsl__ (_http://www.w3.org/1999/XSL/Transform_) fan referència a la plantilla de la pàgina i els de __fo__ (_http://www.w3.org/1999/XSL/Format_) fan referència al format de la pàgina.

+ __Mira si en algun cas és possible prescindir d'algun àlies.__  
Definiria l'àlies __fo__ com a global i això aconseguiria prescindir de varis àlies.
```
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns="http://www.w3.org/1999/XSL/Format"
    >
    <xsl:template match="doc">
        <root>
            <layout-master-set>
                <simple-page-master master-name="A4" page-width="297mm" page-height="210mm" margin-top="1cm" margin-bottom="1cm" margin-left="1cm" margin-right="1cm">
                    <region-body margin="3cm"/>
                    <region-before extent="2cm"/>
                    <region-after extent="2cm"/>
                    <region-start extent="2cm"/>
                    <region-end extent="2cm"/>
                </simple-page-master>
            </layout-master-set>
            <page-sequence master-reference="A4" format="A">
                <flow flow-name="xsl-region-body">
                    <xsl:apply-templates select="para"/>
                </flow>
            </page-sequence>
        </root>
    </xsl:template>
    <xsl:template match="para">
        <block><xsl:value-of select="."/></block>
    </xsl:template>
    <xsl:template match="para[1]">
        <block><inline font-weight="bold"><xsl:value-of select="."/></inline></block>
    </xsl:template>
</xsl:stylesheet>
```

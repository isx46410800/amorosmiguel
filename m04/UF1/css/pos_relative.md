# <u>EXPLICACIÓ POSICIONAMENT RELATIVE</u>
+ Desplaça una caixa respecte de la seva posició original establerta mitjançant el posicionament normal.
El desplaçament de la caixa es controla amb les propietats top, right, bottom i left.

+ Per tant, la propietat top s'empra per a moure les caixes de manera descendent,
la propietat bottom mou les caixes de manera ascendent, la propietat left s'utilitza per a desplaçar les caixes cap a la dreta i la propietat right mou les caixes cap a l'esquerra.

## Primer DIV
```
position: relative;
left: 0%;
top: 0%;
```
> En aquest aquest s'indica que la caixa comenci a la posició 0 tant de top com de left, per tant es veurà a dalt a l'esquerra de nostra pàgina.

## Segon DIV
```
position: relative;
left: 50%;
bottom: 50%;
```
> En aquest cas s'indica que la següent caixa comenci al 50% de left, és a dir, a meitat de pàgina  i després aquest bottom per a què es desplaci la caixa fins a dalt amb aquest 50%.que li falta per a estar a la altura del primer div.

## Tercer DIV
```
position: relative;
left: 0%;
bottom: 50%;
```
> En aquest cas s'indica que la següent caixa comenci al 0% de left per a que estigui enganxada al marge esquerra i es posa bottom 50% per a què pugi la caixa fins a meitat de pàgina enganxada al primer div.

## Quart DIV
```
position: relative;
left: 50%;
bottom: 100%;
```
> En aquest cas s'indica que la següent caixa comenci al 50% de left, és a dir a meitat d pàgina i després aquest bottom per a què pugi el 100% que li falta per arribar a la altura del forat que quedaba.

#### NOTA:
_Si fem els 4 divs de manera estàtica, quedarien un abaix de l'altre. Per tant,
el segon div necesita un 50% de bottom per a pujar a dalt de tot; el tercer necesita un 50% de bottom per pujar i estar a l'altura sota el primer div i el quart necessita un 100% per pujar a l'altura del tercer div._

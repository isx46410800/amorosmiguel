# <u>5.2.receptes.md</u>

__5.2. Feu una còpia del fitxer `5.0.receptes.xml` amb el nom `5.0.receptesAmbErrors.xml`. Introduiu errors expressament ( per exemple canviar valors, on toca booleà posar altre tipus de dades o en la llista enumerada posar un valor no vàlid, etc). Comproveu amb xmllint els missatges d'error que dóna i explique-los al fitxer `5.2.receptes.md`.__

+ Sense canvis:  

```
[isx46410800@miguel-fedora27 scripts]$ xmllint 5.0.receptes.xml --schema 5.0.receptes.xsd --noout
5.0.receptes.xml validates
```

+ Amb canvis:  

[fitxer 5.0.receptesAmbErrors.xml](./scripts/5.0.receptesAmbErrors.xml)

```
[isx46410800@miguel-fedora27 scripts]$ xmllint 5.0.receptesAmbErrors.xml --schema 5.0.receptes.xsd --noout
5.0.receptesAmbErrors.xml:7: element referencia: Schemas validity error : Element 'referencia': 'uno' is not a valid value of the atomic type 'xs:integer'.
5.0.receptesAmbErrors.xml:9: element fecha: Schemas validity error : Element 'fecha': 'la fecha de hoy' is not a valid value of the atomic type 'xs:date'.
5.0.receptesAmbErrors.xml:17: element publicado: Schemas validity error : Element 'publicado': 'no' is not a valid value of the atomic type 'xs:boolean'.
5.0.receptesAmbErrors.xml:18: element clasificacion: Schemas validity error : Element 'clasificacion', attribute 'categoria': [facet 'enumeration'] The value 'recetas de reposterias' is not an element of the set {'recetas de reposteria', 'recetas con carne', 'recetas con fruta'}.
5.0.receptesAmbErrors.xml:18: element clasificacion: Schemas validity error : Element 'clasificacion', attribute 'categoria': 'recetas de reposterias' is not a valid value of the atomic type 'miclasificacion'.
5.0.receptesAmbErrors.xml fails to validate
```

1. S'ha posat text en comptes de un integer
2. S'ha posat text en comptes de una data
3. S'ha posat text en comptes de un booleà
4. S'ha canviat el nom del atribut
5. S'ha canviat el nom del atribut
6. Error al validar

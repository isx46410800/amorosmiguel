# <u>PRÀCTICA 1: XML BEN FORMAT</u>

### Well Formed XML Documents

###### An XML document with correct syntax is called "Well Formed".  
+ __<u>Syntax rules:</u>__
```
XML documents must have a root element
XML elements must have a closing tag
XML tags are case sensitive
XML elements must be properly nested
XML attribute values must be quoted
```

+ __Mira els següents exemples:__
```
<arrel>
< hola>Incorrecte</hola;>
<hola;>Incorrecte</hola;>
<.hola>Incorrecte</.hola>
<:hola>Incorrecte</:hola>
<1hola>Incorrecte</1hola>
<hol.a>Correcte pero no recomanable</hol.a>
<hol:a>Gens recomanable</hol:a>
<xml-el\_que\_sigui>Incorrecte</xml-el\_que\_sigui>
<el\_què\_sigui-xml>Correcte pero no
recomanable<el\_què\_sigui-xml>
<hola=hola>Incorrecte</hola=hola>
</arrel>
```

__1. Son correctes els “tags” següents?__
```
<etiqueta1> #Sí
< etiqueta> #No
<etiqueta 1> #No
<etiqueta > #No
</etiqueta> #Sí
</etiqueta1 > #No
< /etiqueta2> #No
</etiqueta2> #Sí
<étiqueta> #No
```

__2. Implementa un fitxer xml el següent text de manera que es pugui buscar informació segons els
següents camps: destinatari de la comanda, article demanat, direcció entrega, data d'entrega.__
> Comanda pel senyor Joan Delgado Bruc. La comanda es composa d'una
bicicleta A2023. S'ha d'entregar al carrer Vent 4, tercer pis, lletra A, de Barcelona, el dia 19-10-2017.

```
<comanda>
  <destinatari>Joan Delgado Bruc</destinatari>
  <article>bicicleta A2023</article>
  <direccio>Carrer Vent 4, tercer pis, lletra A</direccio>
  <localitat>Barcelona</localitat>
  <entrega>19-10-2017</entrega>
</comanda>
```

__3. Els següents documents estan ben formats?. Corregeix, si cal, els errors i escriu la solució.__

__3.1. Pel·lícules__
```
<?xml version="1.0" encoding="UTF-8"?>
<pelicula>
 <titulo>Con faldas y a lo loco</titulo>
 <director>Billy Wilder</director>
</pelicula>
<pelicula>
 <director>Leo McCarey</director>
 <titulo>Sopa de ganso</titulo>
</pelicula>
<autor />barto</autor>
```
> No és correcte. L'etiqueta autor és incorrecte.
Posar arrel <videoteca>
La solució seria \<autor>Barto\</autor> pero dins de pelicula en tot cas, sol no pot estar.


__3.2. Programes__
```
<?xml version="1.0" encoding="UTF-8"?>
<programas>
 <programa nombre="Firefox" licencia="GPL" licencia="MPL" />
 <programa nombre="OpenOffice.org" licencia=LGPL />
 <programa nombre="Inkscape" licencia="GPL" />
</programas>
```
> No és correcte. Un atribut no es pot repetir en una mateixa etiqueta. També el valor de l'atribut ha d'anar entre "".   
La solució seria posar un altre nom d'atribut i no repetir dues vegades 'licencia' i licencia="LGPL".

__3.3. Mundials de futbol__
```
<?xml version="1.0" encoding="UTF-8"?>
<mundiales-de-futbol>
 <mundial>
 <pais="España" />
 <1982 />
 </mundial>
</mundiales-de-futbol>
```
> No és correcte. No existeix etiqueta correcte en 'pais' i en '1982'.  
La solució seria \<pais>España\</pais> i \<anyo>1982\</anyo>.

__4. Està ben format el següent document? Enumera i explica, si cal, els errors existents.__
```
<?xml version="1.0" encoding="UTF-8"?>
<deportistas>
 <deportista>
 <deporte Atletismo />
 <nombre>Jesse Owens</nombre>
 <deportista>
 <deporte Natación />
 <nombre>Mark Spitz</nombre>
 </deportista>
</deportistas>
```

> No està ben format, hi ha errors d'etiquetes.

+ Correcció:
```
<?xml version="1.0" encoding="UTF-8"?>
<deportistas>
 <deportista>
    <deporte>Atletismo</deporte>
    <nombre>Jesse Owens</nombre>
 </deportista>
 <deportista>
    <deporte>Natación</deporte>
    <nombre>Mark Spitz</nombre>
 </deportista>
</deportistas>
```
  - L'etiqueta deporte està mal posada i el seu valor de manera incorrecte.
  - La primera etiqueta 'deportista' no està tancada.
  - Els dos 'deporte' està l'etiqueta mal feta amb el valor corresponent.

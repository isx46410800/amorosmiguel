#! /bin/bash
#Script xmllint
#Miguel Amoros
#-------------
ERR_NARGS=1
ERR_FILE=2
status=0
OK=0

if ! [ $# -eq 1 ]
then
	echo "Err: num args incorrecte"
	echo "Usage: prog.sh | prog-sh file.xml"
	exit $ERR_NARGS
fi
	
if [ $# -eq 1 -a "$1" == "-h" -o "$1" == "--help" ]
then
	echo "Mostrem l'ajuda"
	exit $OK
fi
	
file=$1
if [ $# -eq 1 ]
then
	if ! [ -f $1 ]
	then
		"Err, $file no es un file"
		exit $ERR_FILE
	fi
fi

echo "Miramos el codigo xml:"
xmllint --noout $file
if [ $? -eq 0 ]
then
	echo "No hay errores de xml"
else
	status=3
fi

echo "Miramos el codigo xml y dtd:"
xmllint --noout --valid $file
if [ $? -eq 0 ]
then
	echo "No hay errores de xml/dtd"
else
	status=4
fi

exit $status


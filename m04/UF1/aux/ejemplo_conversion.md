# ejemplo codificar de utf8 a latin1

## creamos archivo con chars utf8
```
[isx46410800@i05 git]$ echo "codificando en español ascii, comença ja!" > ascii.txt
[isx46410800@i05 git]$ file ascii.txt
ascii.txt: UTF-8 Unicode text
```

## convertimos
```
[isx46410800@i05 git]$ iconv -f UTF-8 -t ISO-8859-15 < utf8.txt > latin1.txt
[isx46410800@i05 git]$ cat latin1.txt
codificando en espa�ol ascii, comen�a ja!
```

## vemos la trasnformacion y numero de bytes
```
[isx46410800@i05 git]$ file utf8.txt latin1.txt
utf8.txt:   UTF-8 Unicode text
latin1.txt: ISO-8859 text
[isx46410800@i05 git]$ ll
-rw-r--r--. 1 isx46410800 hisx2   42 Oct 11 12:08 latin1.txt
-rw-r--r--. 1 isx46410800 hisx2   44 Oct 11 12:03 utf8.txt
```

## numero de bytes
```
[isx46410800@i05 git]$ wc -c utf8.txt latin1.txt
44 utf8.txt
42 latin1.txt
86 total
```

# DTD'S EXTRA

#### 4.1. A la Pràctica2 se us demanava implementar els XML corresponents. En aquesta es demana al mateix doc XML implementar un DTD adient. Fixeu-vos que els scripts tindran el mon que tenien però amb una "b", no canvieu l'original!
+ __2.1.xml: [script amb DTD](./scripts/2.1b.xml)__  
+ __2.2.xml: [script amb DTD](./scripts/2.2b.xml)__  
+ __2.3.xml: [script amb DTD](./scripts/2.3b.xml)__  
+ __2.4.xml: [script amb DTD](./scripts/2.4b.xml)__  
+ __2.5.xml: [script amb DTD](./scripts/2.5b.xml)__  

#### 4.2. Donat aquest XML, implementeu un nou doc amb un DTD adient:
+ __DTD:__
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE cinema[
    <!ELEMENT cinema (pelis*,directors*)>
    <!ELEMENT pelis (peli*)>
    <!ELEMENT peli (#PCDATA)>
    <!ELEMENT directors (director*)>
    <!ELEMENT director (#PCDATA)>
    <!ATTLIST peli codpeli ID #REQUIRED>
    <!ATTLIST director filmografia IDREFS #REQUIRED>
]>
```
+ __XML:__
```
<cinema>
   <pelis>
      <peli codpeli="P1"> Avatar </peli>
      <peli codpeli="P2"> Mystic River </peli>
      <peli codpeli="P3"> The Terminator </peli>
      <peli codpeli="P4"> Titanic </peli>
   </pelis>
   <directors>
      <director filmografia ="P2"> Clint Eastwood </director>
      <director filmografia ="P1 P3 P4"> James Cameron </director>
   </directors>
</cinema>
```

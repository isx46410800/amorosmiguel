#!/usr/bin/python
# Miguel Amoros - isx46410800

"""Expand genshi templates.

Usage: env N=V... python ./render.py TEMPLATE
"""

import os
import sys

from genshi.template import TemplateLoader

TEMPLATE = sys.argv[1]

TPATH = ["./", "/templates"] # directoris on cercar plantilles

loader = TemplateLoader(TPATH) # objecte tipus plantilla

template = loader.load(TEMPLATE) # carrega el objecte

stream = template.generate(**os.environ) # se pasan los valores

sys.stdout.write(stream.render(encoding="UTF-8").decode('utf-8', 'ignore')) # serialitza el flux de dades(vol dir codificar)

sys.exit(0)

# vim:sw=4:ts=4:ai

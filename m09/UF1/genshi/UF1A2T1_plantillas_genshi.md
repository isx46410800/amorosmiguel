# Miguel Amoros - isx46410800

# Pràctiques
### De moment treballarem les plantilles sense usar servidor Web o scriptsCGI.
Simplement expandirem les plantilles en la sortida estàndard i estudiarem el resultat.

__1. Estudia i expandeix (amb render.py) les plantillesde prova situades en el directori aux (fitxers amb extensió
xml).__

`$ python ./render.py t01.xml`
```
[isx46410800@i05 genshi]$ python3 py/render.py xml/t01.xml
<html lang="en">
  <head>
    <title>A Genshi Template</title>
  </head>
  <body>
    <p>These are some of my favorite fruits:</p>
    <ul>
      <li>
        I like apples
      </li><li>
        I like oranges
      </li><li>
        I like kiwis
      </li>
    </ul>
  </body>
</html>
```
`$ title="valor heretat en entorn" python ./render.py t02.xml`
```
[isx46410800@i05 genshi]$ title="valor heretat en entorn" python3 py/render.py xml/t02.xml
<html lang="en">
  <head>
    <title>valor heretat en entorn</title>
  </head>
  <body>
    <p>These are some of my favorite fruits:</p>
    <ul>
      <li>
        I like apples
      </li><li>
        I like oranges
      </li><li>
        I like kiwis
      </li>
    </ul>
  </body>
</html>
```

`$ name=Joan times=10 python ./render.py t03.xml | xmllint --format -`
```
[isx46410800@i05 genshi]$ name=Joan times=10 python3 py/render.py xml/t03.xml | xmllint --format -
<?xml version="1.0"?>
<!-- use times & name -->
<html lang="en">
  <body>
    <h1>Hello Genshi</h1>
    <hr/>
    <ul>
      <li>Hello, Joan!</li>
      <li>Hello, Joan!</li>
      <li>Hello, Joan!</li>
      <li>Hello, Joan!</li>
      <li>Hello, Joan!</li>
      <li>Hello, Joan!</li>
      <li>Hello, Joan!</li>
      <li>Hello, Joan!</li>
      <li>Hello, Joan!</li>
      <li>Hello, Joan!</li>
    </ul>
  </body>
</html>

```
__2. Fes una plantilla que mostri els arguments rebuts en executar a render.py (llista sys.argv)__

Template: [t_arguments.xml](./xml/t_arguments.xml)

```
[isx46410800@i05 genshi]$ python3 py/render.py xml/t_arguments.xml miguel walid jordi
<html lang="en">
  <head>
    <title>Template para mostrar argumentos</title>
  </head>
  <body>
    <p>Estos son los argumentos que pasamos:</p>
    <ul>
      <li>
        Argumento: miguel
      </li><li>
        Argumento: walid
      </li><li>
        Argumento: jordi
      </li>
    </ul>
  </body>
</html>
```
__3. Fes una plantilla que mostri les primeres 16 potencies de 2 (2**1, 2**2, 2**3, etc.).__

Template: [t_potencias.xml](./xml/t_potencias.xml)

```
[isx46410800@i05 genshi]$ python3 py/render.py xml/t_potencias.xml
<html lang="en">
  <head>
    <title>Template para mostrar las 16 primeras potencias de 2</title>
  </head>
  <body>
    <p>Estos son las potencias:</p>
    <ul>
      <li>
        Calculo: 2**1 = 2
      </li><li>
        Calculo: 2**2 = 4
      </li><li>
        Calculo: 2**3 = 8
      </li><li>
        Calculo: 2**4 = 16
      </li><li>
        Calculo: 2**5 = 32
      </li><li>
        Calculo: 2**6 = 64
      </li><li>
        Calculo: 2**7 = 128
      </li><li>
        Calculo: 2**8 = 256
      </li><li>
        Calculo: 2**9 = 512
      </li><li>
        Calculo: 2**10 = 1024
      </li><li>
        Calculo: 2**11 = 2048
      </li><li>
        Calculo: 2**12 = 4096
      </li><li>
        Calculo: 2**13 = 8192
      </li><li>
        Calculo: 2**14 = 16384
      </li><li>
        Calculo: 2**15 = 32768
      </li><li>
        Calculo: 2**16 = 65536
      </li>
    </ul>
  </body>
</html>
```
__4. Fes una plantilla que mostri la taula de multiplicar.__

Template: [t_multiplicacion.xml](./xml/t_multiplicacion.xml)

```
</html>[isx46410800@i05 genshi]$ python3 py/render.py xml/t_multiplicacion.xml 2
<html lang="en">
  <head>
    <title>Template para mostrar una tabla de multiplicar</title>
  </head>
  <body>
    <p>Esta es la tabla de multiplicar del 2:</p>
    <ul>
      <li>
        Calculo: 0 x 2 = 0
      </li><li>
        Calculo: 1 x 2 = 2
      </li><li>
        Calculo: 2 x 2 = 4
      </li><li>
        Calculo: 3 x 2 = 6
      </li><li>
        Calculo: 4 x 2 = 8
      </li><li>
        Calculo: 5 x 2 = 10
      </li><li>
        Calculo: 6 x 2 = 12
      </li><li>
        Calculo: 7 x 2 = 14
      </li><li>
        Calculo: 8 x 2 = 16
      </li><li>
        Calculo: 9 x 2 = 18
      </li><li>
        Calculo: 10 x 2 = 20
      </li>
    </ul>
  </body>
</html>
```

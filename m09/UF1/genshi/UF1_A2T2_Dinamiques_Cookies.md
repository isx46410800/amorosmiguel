#### Miguel Amoros - isx46410800
# <u>Pàgines dinàmiques</u>

## Pràctiques
__1. Fes una pàgina dinàmica que mostri les primeres 16 potencies de 2__

`(2**1, 2**2, 2**3, etc).`

Script: [potencias](./py/potencias)  
Template: [t_potencias.xml](./xml/t_potencias.xml)

__2. Fes una pàgina dinàmica que mostri la taula de multiplicar.__

Script: [multiplicar](./py/multiplicar)  
Template: [t_multiplicacion_entorn.xml](./xml/t_multiplicacion_entorn.xml)

# <u>Galetes (cookies)</u>

## Pràctiques

__3. Fes una pàgina que cada vegada que es visiti faci servir un color de fons a triar a l’atzar entre aqua, black, blue, fuchsia, gray, green, lime, maroon, navy, olive, purple, red, silver, teal, white, i yellow. En totes les pràctiques per generar dinàmicament el valor de l’atribut style usa sempre la directiva py:attrs.__

Script: [ex3_cookies](./A2T2/ex3_cookies)  
Template: [t_ex3_cookies.xml](./A2T2/t_ex3_cookies.xml)

__4. Fes una pàgina que en visitar-la per primera vegada tingui el color groc de fons de pàgina, en la següent visita de color verd, i que en posteriors visites vagi alternant entre els dos colors. Pots encapsular la gestió de les galetes amb un script implementat segons aquest pseudocodi:__
```
1.  Si no hem rebut la galeta…
2.  …afegir una galeta amb el primer color a usar a la resposta i retornar el nom d’aquest color
3.  i si la hem rebut triar el següent color segons el valor de la galeta rebuda, afegir-la de nou a la resposta amb el nou color i retornar el nom d’aquest color.
```

Script: [ex4_cookies](./A2T2/ex4_cookies)  
Template: [t_ex4_cookies.xml](./A2T2/t_ex4_cookies.xml)


__5. Fes una pàgina que en visitar-la per primera vegada retorni un missatge publicitari, amb un enllaç o botó a la mateixa pàgina. En clicar-lo s’ha de mostra la pàgina definitiva i no el missatge de propaganda. Pots encapsular la gestió de les galetes amb un script implementat segons aquest pseudocodi:__
```
1.  Si no hem rebut la galeta…
2.  …afegir una galeta a la resposta (el valor no importa) i retornar True
3.  i si la hem rebut retornar False.
```

Script: [ex5_cookies](./A2T2/ex5_cookies)  
Template: [t_ex5_cookies.xml](./A2T2/t_ex5_cookies.xml)


__6. Fes una pàgina que cada vegada que es visiti faci servir, de forma rotativa, un color de fons de la llista aqua, black, blue, fuchsia, gray, green, lime, maroon, navy, olive, purple, red, silver, teal, white, i yellow. Pots encapsular la gestió de les galetes amb un script implementat segons aquest pseudocodi:__
```
1.  Podem codificar els colors amb els enters des de 0 a 15.
2.  Si no hem rebut la galeta…
3.  …afegir galeta a la resposta amb el valor 0 i retornar el nom del color 0
4.  i si la hem rebut incrementar codi de color (mòdul 16:
n = (n + 1) % 16), afegir una galeta amb el nou codi de color i retornar el seu nom.
```
> _Important: per provocar l’expiració de les galetes simplement tancarem totes les finestres del navegador._

Script: [ex6_cookies](./A2T2/ex6_cookies)  
Template: [t_ex6_cookies.xml](./A2T2/t_ex6_cookies.xml)

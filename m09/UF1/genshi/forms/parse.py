#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Miguel Amoros - isx46410800

import urllib, os

def parse(s):
    """Parse the url-encoded string s returning a dictionary.
    Caveat: does not support duplicated keys, etc.
    """
    if s == "": return {}
    pairs = s.split("&")
    pairs = [(k, urllib.parse.unquote_plus(v))
              for (k, v) in [s.split("=", 1) for s in pairs]]
    return dict(pairs)

form = parse(os.environ["QUERY_STRING"])

print (form)

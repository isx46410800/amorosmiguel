#!/usr/bin/python
# -*- coding: utf-8 -*-
import urllib, os

# Paraules a exportar
__all__ = (
    'parse',
    'decode',
)

def parse(s):
    """Parse the url-encoded string s returning a dictionary.
    Caveat: does not support duplicated keys, etc.
    """
    if s == "": return {}
    pairs = s.split("&")
    pairs = [(k, decode(v))
              for (k, v) in [s.split("=", 1) for s in pairs]]
    return pairs


def decode(s):
    '''Assumim que no hi ha errors'''
    s = s.replace('+',' ')
    i = 0
    result = ''
    while i < len(s):
        if s[i] == '%':
            h = s[i+1:i+3]
            integer = int(h,16)
            codi = chr(integer)
            result = result + codi
            i = i + 3
        else:
            result = result + s[i]
            i = i + 1
    return result

# Miguel Amoros - isx46410800


# libreria importada
>>> import urllib.parse

# quote plus cambio de utf8 a query y espacios por +
>>> urllib.parse.quote_plus("Hello  world")
'Hello++world'
>>> urllib.parse.quote_plus("Hello world")
'Hello+world'
>>> urllib.parse.quote_plus("Hello+world!")
'Hello%2Bworld%21'
>>> urllib.parse.quote_plus(urllib.parse.quote_plus("Hello+world!"))
'Hello%252Bworld%2521'

# unquote plus cambio query a utf8 y + por espacio
>>> urllib.parse.unquote_plus("Hello+world")
'Hello world'
>>> urllib.parse.unquote_plus("/El+Ni%C3%B1o/")
'/El Niño/'
>>>

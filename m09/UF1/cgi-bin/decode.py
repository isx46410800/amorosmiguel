#!/usr/bin/python
# -*- coding:utf-8-*-
# Miguel Amoros - isx46410800

#importamos libreria
import os, sys
import cgitb; cgitb.enable()
import urllib

#creamos la funcion decode
def decode(url):
    '''
    Implementa partint de zero una funció
    de nom decode equivalent a urllib.unquote_plus.
    '''
    url = url.replace("+"," ")
    url = url.replace("\\xc3\\xb","ñ")
    url = url.replace("%C3%A7", "ç")
    url = url.replace("%21", "!")
    url = url.replace("%3F", "?")
    url = url.replace("%C2%BF", "¿")
    url = url.replace("%C2%A1", "¡")
    url = url.replace("%25", "%")
    url = url.replace("%20", " ")

    return url

write = sys.stdout.write

write('Content-Type: text/plain; charset=UTF-8\r\n')
write('\r\n')
#os.environ['QUERY_STRING'] = 'a=1&b=3&c=4'
camps = os.environ['QUERY_STRING'].split("&")

for i in range(len(camps)):
    camps[i] = camps[i].split("=", 1)

for i in range(len(camps)):
    camps[i] = (camps[i][0], decode(camps[i][1]))

for (k, v) in camps:
    write("%s = %s\n" % (k, v))

#COMPROBACION
## En nuestro ejercicio de decode_prueba.py el resultado de la conversión es:
#print(decode("%C2%BF%C2%A1holaa%21%3F")) #¿¡holaa!?

##Ejecutamos en terminal
#[isx46410800@i05 cgi-bin]$ REQUEST_METHOD=GET QUERY_STRING='a=%C2%BF%C2%A1holaa%21%3F' ./decode.py
#Content-Type: text/plain; charset=UTF-8
#a = ¿¡holaa!?

#por web
#http://localhost/cgi-bin/decode.py?a=hola%20que%20tal&b=%C2%BF%C2%A1holaa%21%3F

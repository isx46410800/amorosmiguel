#!/usr/bin/python
# -*- coding:utf-8-*-
# Miguel Amoros - isx46410800

import os, sys
import cgitb; cgitb.enable()
import urllib

write = sys.stdout.write

write('Content-Type: text/plain; charset=UTF-8\r\n')
write('\r\n')
#os.environ['QUERY_STRING'] = 'a=1&b=3&c=4'
camps = os.environ['QUERY_STRING'].split("&")

for i in range(len(camps)):
    camps[i] = camps[i].split("=", 1)

for i in range(len(camps)):
    camps[i] = (camps[i][0], urllib.unquote_plus(camps[i][1]))

for (k, v) in camps:
    write("%s = %s\n" % (k, v))

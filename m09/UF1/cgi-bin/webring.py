#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Miguel Amoros - isx46410800

import os, sys
import cgitb; cgitb.enable()
import random

write = sys.stdout.write

llista_URL = ['localhost/cgi-bin/1.py','localhost/cgi-bin/2.py','localhost/cgi-bin/3.py']

camps = os.environ['QUERY_STRING'].split("&")

if camps[0] == '':
	URL = random.sample(llista_URL,1)
	write('Content-Type: text/html; charset=UTF-8\r\n')
	write('location: http://%s \r\n' %(URL[0]))
	write('\r\n')
	sys.exit(0)

for i in range(len(camps)):
    camps[i] = camps[i].split("=", 1)

valor= camps[0][1]

f = open("/var/www/tmp/num","r")

num = (f.read())

f.close()

num = int(num)

num_ant = int(num)


if valor != 'esquerra' and valor != 'dreta' and valor != 'atzar':
	write('Content-Type: text/plain; charset=UTF-8\r\n')
	write('\r\n')
	write('la sintaxy es la seguent: ?visita=direccio')
	write('\r\n')
	write('en direccio ha de introduir: detra, esquerra o atzar')
	sys.exit(0)

elif valor == 'atzar':
	URL = random.sample(llista_URL,1)
	write('Content-Type: text/html; charset=UTF-8\r\n')
	write('Referer: http://%s \r\n' %(llista_URL[num_ant]))
	write('location: http://%s \r\n' %(URL[0]))
	write('\r\n')
	sys.exit(0)

if valor == 'dreta':

	if num == 2:
		num = 0

	else:
		num = num + 1
else:

	if num == 0:
		num = 2

	else:
		num = num - 1

f2 = open("/var/www/tmp/num","w")

f2.write(str(num))

f2.close()

write('Content-Type: text/html; charset=UTF-8\r\n')
write('Referer: http://%s \r\n' %(llista_URL[num_ant]))
write('location: http://%s \r\n' %(llista_URL[num]))
write('\r\n')
sys.exit(0)

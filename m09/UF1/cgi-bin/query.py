#!/usr/bin/python
# -*- coding: utf-8 -*-
# Miguel Amoros - isx46410800

#importamos librerias
import os, sys
import cgitb; cgitb.enable()
import urllib

#funcion parse
def parse(s):
    """Parse the url-encoded string s returning a dictionary.
    Caveat: does not support duplicated keys, etc.
    """
    if s == "": return {}
    pairs = s.split("&")
    pairs = [(k, urllib.unquote_plus(v)) for (k, v) in [s.split("=", 1) for s in pairs]]
    dict = {}
    # si existe o no existe el campo, se añade key/valor o se añade a dentro de la key ya nombrada
    for camp in pairs:
        if camp[0] not in dict.keys():
            d[camp[0]] = [camp[1]]
        else:
            d[camp[0]].append(camp[1])
    return dict

#cabeceras
write = sys.stdout.write
write('Content-Type: text/plain; charset=UTF-8\r\n')
write('\r\n')

#resultado
form = parse(os.environ["QUERY_STRING"])
print(form)

#comprobacion
#http://localhost/cgi-bin/query.py?a=hola&b=bebe&b=bebe&c=que%tal
#{'a': ['hola'], 'c': ['que%tal'], 'b': ['bebe', 'bebe']}

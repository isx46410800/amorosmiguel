#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Miguel Amoros - isx46410800

import cgi, cgitb, sys, urllib.parse, os
cgitb.enable()
# Debug
write = sys.stdout.write


# headers
write("Content-Type: text/plain; charset=UTF-8\r\n") # or "text/html"...
write("\r\n")

file = ("/var/www/tmp/contador.log")

#abrimos fichero
fluxe = open(file, "r")
contador = fluxe.readline()
fluxe.close()

#volvemos abrir para sobreescribir el file anterior
fluxe = open(file, "w")
fluxe.write("%i" %(int(contador)+1))
fluxe.close()

# mostrar
print ("Numero total de visitas: %s" %(contador))

sys.exit(0)

#comprobacion:
# [isx46410800@i05 cgi-bin]$ sudo python3 count_.py
# Content-Type: text/plain; charset=UTF-8
#
# Numero total de visitas: 0
#
# [isx46410800@i05 cgi-bin]$ sudo python3 count_.py
# Content-Type: text/plain; charset=UTF-8
#
# Numero total de visitas: 1
# [isx46410800@i05 cgi-bin]$ sudo python3 count_.py
# Content-Type: text/plain; charset=UTF-8
#
# Numero total de visitas: 2

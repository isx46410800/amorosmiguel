#!/usr/bin/python
# -*- coding:utf-8-*-
# Miguel Amoros - isx46410800
#Fes un script de nom salta que en ser visitat redirigeixi a una
#URL triada a l’atzar d’una llista (has de redirigir amb la capçalera
#escrita per l’script Location: URL). Quin tipus de resposta de
#redirecció ha fet el servidor?
#---------------------------------------------------------------------
#importamos libreria random
import random, sys

# cabeceras
write = sys.stdout.write

lista_urls = ['https://as.com/', 'https://www.marca.com/', 'https://www.google.com/']

#cogemos con randrange para que vaya de 0 a 2 , ya que len es 3
#randrange no incluye el ultimo elemento
url = lista_urls[random.randrange(0, len(lista_urls))]

#redirige a Location: url
write('Content-Type: text/plain; charset=UTF-8\r\n')
write("Location: %s\r\n" % url)
write("\r\n")

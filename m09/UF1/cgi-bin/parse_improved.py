#!/usr/bin/python
# Miguel Amoros - isx46410800

#importamos librerias
import os, sys
import cgitb; cgitb.enable()
import urllib

#funcion parse
def parse(s):
    """Parse the url-encoded string s returning a dictionary.
    Caveat: does not support duplicated keys, etc.
    """
    if s == "": return {}
    pairs = s.split("&")

    for val in pairs:
      if val == '' or '=' not in val:
        raise ValueError('bad query string')

    pairs = [(k, urllib.unquote_plus(v))
              for (k, v) in [s.split("=", 1) for s in pairs]]
    return pairs

#cabeceras
write = sys.stdout.write
write('Content-Type: text/plain; charset=UTF-8\r\n')
write('\r\n')

#resultados
form = parse(os.environ["QUERY_STRING"])
print form

#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Miguel Amoros - isx46410800

import cgi, cgitb, sys, urllib.parse, cgitb.enable(), random

lista_urls = ['http://127.0.0.2/pagina1.html', 'http://127.0.0.2/pagina2.html', 'http://127.0.0.2/pagina3.html']

direccio = ["esquerra", "dreta"]

aleatorio = random.choice(direccio)

#desglose de campos del query string
camps = os.environ['QUERY_STRING'].split("=")

#metemos en un diccionario variable=valor
dict = {}

referer = 'http://127.0.0.2/pagina1.html'

for i in range(len(camps)):
    dict[camps[i][0]] = camps[i][1]

write('Content-Type: text/plain; charset=UTF-8\r\n')
write('\r\n')

if dict['visita'] == 'esquerra':
    write('referer: %s' % (lista_urls[0]))
    write("Location: %s\r\n" % lista_urls[1])
elif dict['visita'] == 'dreta':
    write('referer: %s' % (lista_urls[1]))
    write("Location: %s\r\n" % lista_urls[2])
elif dict['visita'] == 'atzar':
    write('referer: %s' % (lista_urls[1]))
    write("Location: %s\r\n" % aleatorio)

else:
    write("error")

write('Content-Type: text/plain; charset=UTF-8\r\n')
write('Referer: %s' % (referer) )
write("Location: %s\r\n" % lista_urls[0])


sys.exit(0)

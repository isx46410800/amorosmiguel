#!/usr/bin/python
# -*- coding:utf-8-*-
# Miguel Amoros - isx46410800

#importamos libreria
import urllib

#creamos la funcion decode
def decode(url):
    '''
    Implementa partint de zero una funció
    de nom decode equivalent a urllib.unquote_plus.
    '''
    url = url.replace("+"," ")
    url = url.replace("\\xc3\\xb","ñ")
    url = url.replace("%C3%A7", "ç")
    url = url.replace("%21", "!")
    url = url.replace("%3F", "?")
    url = url.replace("%C2%BF", "¿")
    url = url.replace("%C2%A1", "¡")
    url = url.replace("%25", "%")
    url = url.replace("%20", " ")

    return url

#creamos la funcion decode
def unquote_plus(url):
    '''
    Implementa partint de zero una funció
    de nom decode equivalent a urllib.unquote_plus.
    '''
    return urllib.unquote_plus(url)

#ejemplos de decode
print(decode("https://wwww.google.com/nombre=miguel&apellidos=amoros+moret&dni=46410800C"))
print(decode("Hello+world"))
print(decode("Madrid+13+champions+Barça+5"))
print(decode("El+niño"))
print(decode("el+bar%C3%A7a")) #el barça
print(decode("%C2%BF%C2%A1holaa%21%3F")) #¿¡holaa!?

#ejemplos de unquote_plus
print(unquote_plus("Hello+world"))
print(unquote_plus("Madrid+13+champions+Barça+5"))
print(unquote_plus("https://wwww.google.com/nombre=miguel&apellidos=amoros+moret&dni=46410800C"))
print(unquote_plus("El+niño"))
print(unquote_plus("el+bar%C3%A7a")) #el barça
print(unquote_plus("%C2%BF%C2%A1holaa%21%3F")) #¿¡holaa!?

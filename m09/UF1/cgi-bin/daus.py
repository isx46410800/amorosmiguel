#!/usr/bin/python
# -*- coding:utf-8-*-
# Miguel Amoros - isx46410800

#Fes un script de nom daus que en ser visitat retorni el resultat
#de simular una tirada de daus (usa el mòdul de Python random per
#obtenir valors a l’atzar).
#------------------------------------------------------------------
#importamos libreria random
import random, sys

# cabeceras
write = sys.stdout.write
write('Content-Type: text/plain; charset=UTF-8\r\n')
write('\r\n')

# modulo random con funcion randint: incluye un numero del 1 al 6 incluidos
print random.randint(1,6)

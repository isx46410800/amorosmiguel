#!/usr/bin/python
# -*- coding:utf-8-*-
# Miguel Amoros - isx46410800

import os, sys
import cgitb; cgitb.enable()

write = sys.stdout.write

write('Content-Type: text/plain; charset=UTF-8\r\n')
write('\r\n')
#os.environ['QUERY_STRING'] = 'a=1&b=3&c=4'
for (k, v) in os.environ.iteritems():
    write("%s = %s\n" % (k, v))

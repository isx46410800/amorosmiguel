#!/usr/bin/python
# -*- coding:utf-8-*-
# Miguel Amoros - isx46410800

#Fes un script de nom calcula que en ser visitat amb una query
#string amb la forma ?N=nombre&M=nombre&OP=operació retorni el
#resultadoultat d’aplicar l’operació OP (suma, resultadota, multiplicació o
#divisió codificades amb un sol caràcter) als nombresultado N i M.
#------------------------------------------------------------------
#importamos librerias
import random
import os, sys
import cgitb; cgitb.enable()
import urllib

#cabeceras
write = sys.stdout.write
write('Content-Type: text/plain; charset=UTF-8\r\n')
write('\r\n')

#desglose de campos del query string
camps = os.environ['QUERY_STRING'].split("&")

for i in range(len(camps)):
    camps[i] = camps[i].split("=", 1)

# metemos en un diccionario variable=valor
dict = {}

for i in range(len(camps)):
    dict[camps[i][0]] = camps[i][1]

if dict['OP'] == '+':
    resultado = int(dict['N'])+int(dict['M'])
    write(str(resultado))

elif dict['OP'] == '-':
    resultado = int(dict['N'])-int(dict['M'])
    write(str(resultado))

elif dict['OP'] == '*':
    resultado = int(dict['N'])*int(dict['M'])
    write(str(resultado))

elif dict['OP'] == '/':
    resultado = int(dict['N'])/int(dict['M'])
    write(str(resultado))
else:
    write("Operacion incorrecta")

sys.exit(0)

#comprobacion
#http://localhost/cgi-bin/calcula.py?N=2&M=3&OP=*
#
#http://localhost/cgi-bin/calcula.py?N=10&M=5&OP=/
#2
